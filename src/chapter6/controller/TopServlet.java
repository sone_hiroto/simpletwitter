package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        boolean isShowMessageForm = false;
        User user = (User) request.getSession().getAttribute("loginUser");
        String userId = request.getParameter("user_id");
        //選択した日付を取得
        String start = request.getParameter("start");
        String end = request.getParameter("end");

        if (user != null) {
            isShowMessageForm = true;
        }

        List<UserMessage> messages = new MessageService().select(userId, start, end);

        //コメント情報を取得してtop.jspに渡す
        List<UserComment> comments = new CommentService().select();


        request.setAttribute("messages", messages);
        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.setAttribute("comments", comments);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}